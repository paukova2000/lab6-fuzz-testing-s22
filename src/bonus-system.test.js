import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe('Bonus system tests', () => {
  let app;
  console.log("Tests started");

  test('Standard 9000',  (done) => {
    let a = "Standard";
    let b = 9000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 1);
    done();
  });

  test('Standard 10000',  (done) => {
    let a = "Standard";
    let b = 10000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 1.5);
    done();
  });

  test('Standard 49000',  (done) => {
    let a = "Standard";
    let b = 49000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 1.5);
    done();
  });

  test('Standard 50000',  (done) => {
    let a = "Standard";
    let b = 50000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 2);
    done();
  });

  test('Standard 99000',  (done) => {
    let a = "Standard";
    let b = 99000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 2);
    done();
  });

  test('Standard 100000',  (done) => {
    let a = "Standard";
    let b = 100000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 2.5);
    done();
  });

  test('Standard 110000',  (done) => {
    let a = "Standard";
    let b = 110000;
    expect(calculateBonuses(a, b)).toEqual(0.05 * 2.5);
    done();
  });

  test('Premium 900',  (done) => {
    let a = "Premium";
    let b = 900;
    expect(calculateBonuses(a, b)).toEqual(0.1);
    done();
  });

  test('Diamond 900',  (done) => {
    let a = "Diamond";
    let b = 900;
    expect(calculateBonuses(a, b)).toEqual(0.2);
    done();
  });

  test('test 900',  (done) => {
    let a = "test";
    let b = 900;
    expect(calculateBonuses(a, b)).toEqual(0);
    done();
  });

  console.log('Tests Finished');
})